package me.ray;

import me.ray.utils.HTTPUtils;
import me.ray.utils.PingUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Scheduler {

    private String email;
    private String password;
    private String newName;
    private Date date;
    private Rayper rayper;
    private String id;

    public Scheduler(final Rayper rayper) {
        this.rayper = rayper;

        final int prepareTime = 10 * 1000;

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(date != null) {
                    long currentTime = System.currentTimeMillis();
                    long difference = date.getTime() - currentTime;

                    int maxPing = 0;
                    int preDelay = 15 * 1000;
                    boolean pinged = false;

                    if(difference <= preDelay) {
                        if(!pinged) {
                            pinged = true;

                            try {
                                maxPing = PingUtil.getMaxPing(50);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            System.out.println("Resolved Max Ping. (" + maxPing + ")");
                        }
                    }

                    if(difference <= prepareTime + maxPing) {
                            date = null;
                            rayper.getSnipeUtils().presnipe(email, password, newName, maxPing, prepareTime, Scheduler.this, id);
                            this.cancel();
                    }
                }
            }
        }, 0, 1);
    }

    public void addSnipe(String email, String password, String newName, Date date, String id) {
        this.email = email;
        this.password = password;
        this.newName = newName;
        this.date = date;
        this.id = id;
    }

    public boolean isValid(Date date) {
        return date.after(Calendar.getInstance().getTime());
    }

}
