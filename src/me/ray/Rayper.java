package me.ray;

import me.ray.utils.HTTPUtils;
import me.ray.utils.SnipeUtils;
import org.apache.sling.commons.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Rayper {

    private Scheduler scheduler = new Scheduler(this);
    private SnipeUtils snipeUtils = new SnipeUtils();
    private HTTPUtils httpUtils = new HTTPUtils();

    public static void main(String[] args) {
        Rayper rayper = new Rayper();

        try {
            rayper.inputData(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void inputData(String[] args) throws Exception {
        String email, password, newName, id;
        Date date;

        String[] dataSet = args[0].split(" ");

        email = dataSet[0];
        password = dataSet[1];
        newName = dataSet[3];
        id = dataSet[2];

        String[] dateSet = snipeUtils.getDate(newName).split(" ");

        StringBuilder timeInfoBuilder = new StringBuilder();

        try {
            timeInfoBuilder.append(dateSet[0] + ":");
            timeInfoBuilder.append(dateSet[1] + ":");
            timeInfoBuilder.append(dateSet[2] + ":");
            timeInfoBuilder.append(dateSet[3] + ":");
            timeInfoBuilder.append(dateSet[4] + ":");
            timeInfoBuilder.append(dateSet[5]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Invalid arguments!");

            return;
        }

        String timeInfo = timeInfoBuilder.toString();

        DateFormat format = new SimpleDateFormat("MM:dd:yyyy:HH:mm:ss");
        Date newDate = format.parse(timeInfo);
        newDate.setTime(newDate.getTime() + 3600000);
        date = newDate;

        if(scheduler.isValid(date)) {
            scheduler.addSnipe(email, password, newName, date, id);
        } else {
            System.out.println("That date is invalid!");

            return;
        }
    }

    public SnipeUtils getSnipeUtils() {
        return snipeUtils;
    }
}
