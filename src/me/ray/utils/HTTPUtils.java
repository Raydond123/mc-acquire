package me.ray.utils;

import org.apache.sling.commons.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class HTTPUtils {

    // the following method is a modified copy of httpRequest from http://stackoverflow.com/questions/23457364/java-minecraft-authentication
    public JSONObject httpRequest(URL url, String content) throws Exception {
        byte[] contentBytes = content.getBytes("UTF-8");

        URLConnection connection = url.openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestProperty("Accept-Charset", "UTF-8");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Content-Length", Integer.toString(contentBytes.length));

        OutputStream requestStream = connection.getOutputStream();
        requestStream.write(contentBytes, 0, contentBytes.length);
        requestStream.close();

        JSONObject response;
        BufferedReader responseStream;
        if (((HttpURLConnection) connection).getResponseCode() == 200) {
            responseStream = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        } else {
            responseStream = new BufferedReader(new InputStreamReader(((HttpURLConnection) connection).getErrorStream(), "UTF-8"));
        }

        response = new JSONObject(responseStream.readLine());
        responseStream.close();

        if (((HttpURLConnection) connection).getResponseCode() != 200) {
            //Failed to login (Invalid Credentials or whatever)
        }

        return response;
    }

    // the following method is a modified copy of MakeJSONRequest from http://stackoverflow.com/questions/23457364/java-minecraft-authentication
    public String makeJSONRequest(String username, String password) {
        try {
            JSONObject json1 = new JSONObject();
            json1.put("name", "Minecraft");
            json1.put("version", 1);

            JSONObject json = new JSONObject();
            json.put("agent", json1);
            json.put("username", username);
            json.put("password", password);
            return json.toString();
        } catch (Exception e) {
            System.out.println(e);
            return e.getMessage();
        }
    }

    public static String getUrlSource(String url)
            throws IOException
    {
        URL yahoo = new URL(url);
        HttpsURLConnection yc = (HttpsURLConnection)yahoo.openConnection();
        yc.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));

        StringBuilder a = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            a.append(inputLine);
        }
        in.close();

        return a.toString();
    }

}
