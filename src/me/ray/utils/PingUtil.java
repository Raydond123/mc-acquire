package me.ray.utils;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PingUtil {

    public static int getMaxPing(int amtPings) throws Exception {
        List<Long> pings = new ArrayList<>();

        for(int i = 0; i < amtPings; i++) {
            long ping = getPing("https://account.mojang.com");

            pings.add(ping);

            if(i == amtPings - 1) {
                Collections.sort(pings);
                return new BigDecimal(pings.get(pings.size() - 1)).intValueExact();
            }
        }

        return -1;
    }

    public static long getPing(String url) throws IOException {
        long result;

        try {
            long start = System.currentTimeMillis();

            URL link = new URL(url);

            HttpsURLConnection connection = (HttpsURLConnection) link.openConnection();
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("GET");

            connection.connect();

            long end = System.currentTimeMillis();

            result = end - start;
        } catch (IOException e) {
            result = -1;
        }

        return result;
    }

}
