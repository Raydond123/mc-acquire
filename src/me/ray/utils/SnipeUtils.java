package me.ray.utils;

import me.ray.Scheduler;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class SnipeUtils {

    public void presnipe(String username, String password, String newName, int requests, int delay, Scheduler scheduler, String id) {
        try {
            HttpClient httpclient = HttpClientBuilder.create().disableRedirectHandling().build();
            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

            HttpPost login = new HttpPost("https://account.mojang.com/login"),
                    changeName = new HttpPost("https://account.mojang.com/me/renameProfile/" + id);

            List<NameValuePair> paramsLogin = new ArrayList<>(3);
            paramsLogin.add(new BasicNameValuePair("username", username));
            paramsLogin.add(new BasicNameValuePair("password", password));
            paramsLogin.add(new BasicNameValuePair("remember", "true"));

            login.setEntity(new UrlEncodedFormEntity(paramsLogin, "UTF-8"));

            httpclient.execute(login, httpContext);

            List<NameValuePair> paramsChangeName = new ArrayList<>(3);
            paramsChangeName.add(new BasicNameValuePair("newName", newName));
            paramsChangeName.add(new BasicNameValuePair("password", password));
            paramsChangeName.add(new BasicNameValuePair("authenticityToken", ""));

            changeName.setEntity(new UrlEncodedFormEntity(paramsChangeName, "UTF-8"));

            httpclient.execute(changeName, httpContext);
            System.out.println(cookieStore.getCookies().toString());
            /*
            String authenticityToken = cookieStore.getCookies().toString().split("&___AT=")[1].substring(0, 40);

            paramsChangeName.set(2, new BasicNameValuePair("authenticityToken", authenticityToken));
            changeName.setEntity(new UrlEncodedFormEntity(paramsChangeName, "UTF-8"));

            RequestConfig requestConfig = RequestConfig.custom()
                    .setSocketTimeout(5000)
                    .setConnectTimeout(5000)
                    .setConnectionRequestTimeout(5000)
                    .build();

            final CloseableHttpAsyncClient httpAsyncClient = HttpAsyncClients.custom()
                    .setMaxConnTotal(requests)
                    .setMaxConnPerRoute(requests)
                    .setDefaultCookieStore(cookieStore)
                    .setDefaultRequestConfig(requestConfig)
                    .build();

            Thread.sleep(delay);

            httpAsyncClient.start();
            HttpContext tempContext = httpContext;

            for (int i = 0; i < requests; i++) {
                System.out.println("Sending request " + (i + 1));

                httpAsyncClient.execute(changeName, httpContext, null);
                httpContext = tempContext;

                Thread.sleep(1);
            }

            System.out.println("Sniped: " + newName);

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        httpAsyncClient.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, 5000);
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDate(String name) throws Exception {
        boolean validName = false;
        String nameResponse = "";
        while (!validName)
        {
            nameResponse = HTTPUtils.getUrlSource("https://www.namemc.com/s?" + name);
            if (nameResponse.contains("availability-time")) {
                validName = true;
            } else {
                System.out.println("Invalid name!");
            }
        }
        String rawTime = nameResponse.split("availability-time")[1].split(">")[0];

        String unformattedTime = "";
        for (char c : rawTime.toCharArray())
        {
            if (c == '<') {
                break;
            }
            unformattedTime = unformattedTime + c;
        }
        unformattedTime = unformattedTime.substring(3, unformattedTime.length() - 11);
        unformattedTime = unformattedTime.split(" data-now")[0];
        unformattedTime = unformattedTime.substring(9, unformattedTime.length() - 6);
        unformattedTime = unformattedTime.replace("T", ":");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        Date date = format.parse(unformattedTime);
        date = new Date(date.getTime() - 21600000L);

        SimpleDateFormat outputFormat = new SimpleDateFormat("MM dd yyyy HH mm ss");

        return outputFormat.format(date);
    }

}
